package trongtuyen.a1453057_1453059;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

/**
 * Created by trong on 02-Nov-16.
 */

public class CustomIconLabelAdapter extends  ArrayAdapter<String>{
    Context context;
    Integer[] thumbnails;
    String[] items;
    int PrevID = 100;

    public CustomIconLabelAdapter( Context context, int layoutToBeInflated, String[] items, Integer[] thumbnails)
    {
        super(context, R.layout.custom_row_icon_label,items);
        this.context = context;
        this.thumbnails = thumbnails;
        this.items = items;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View row = inflater.inflate(R.layout.custom_row_icon_label, null);

        TextView label = (TextView) row.findViewById(R.id.label);
        ImageView icon = (ImageView) row.findViewById(R.id.icon);

        label.setText(items[position]);
        icon.setImageResource(thumbnails[position]);

        RadioButton rButton = (RadioButton) row.findViewById(R.id.check);
        rButton.setChecked(position==PrevID);
        rButton.setTag(position);
        rButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                PrevID = (Integer) v.getTag();
                String sText = "Position: "+position;
                Toast.makeText(context, sText,Toast.LENGTH_SHORT).show();
                notifyDataSetChanged();
            }
        });
        return(row);
    }

}

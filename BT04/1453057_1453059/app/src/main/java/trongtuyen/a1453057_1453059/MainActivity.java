package trongtuyen.a1453057_1453059;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.app.ListActivity;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

    String[]items={"Pic01", "Pic02", "Pic03", "Pic04", "Pic05", "Pic06", "Pic07", "Pic08", "Pic09", "Pic10"};
    Integer[]thumbnails ={R.drawable.p01, R.drawable.p02, R.drawable.p03, R.drawable.p04, R.drawable.p05, R.drawable.p06, R.drawable.p07, R.drawable.p08, R.drawable.p09, R.drawable.p10};
    //ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CustomIconLabelAdapter adapter = new CustomIconLabelAdapter(
                this,
                R.layout.custom_row_icon_label,
                items,
                thumbnails
        );
        setListAdapter(adapter);




    }
    @Override
    protected void onPause(){
        super.onPause();
    }

}
